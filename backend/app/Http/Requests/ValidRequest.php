<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:30'],
            'prefecture' => ['required' , 'integer' , 'between:1,47'],
            'address' => ['required', 'max:30'],
            'address_second' => 'max:30',
            'tel' => ['required' , 'regex:/^[0-9\-]+$/'],
            'sex' => ['required' , 'integer' , 'between:1,2'],
            'comment' => 'max:100',
            'hobby' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '【名前】は必須項目です。',
            'name.max' => '【名前】は３０文字以内で入力してください。    ',
            'prefecture.required' => '【都道府県】は必須項目です。',
            'prefecture.integer' => '【都道府県】に不正な値が入力されています。',
            'prefecture.between' => '【都道府県】存在する項目から選択してください。',
            'address.required' => '【住所】は必須項目です。',
            'address.max' => '【住所】は３０文字以内で入力してください。',
            'address_second.max' => '【住所2】は３０文字以内で入力してください。',
            'tel.required' => '【電話番号】は必須項目です。',
            'tel.regex' => '【電話番号】は半角数字、またはハイフンのみで入力してください。',
            'sex.required' => '【性別】は必須項目です。',
            'sex.integer' => '【性別】に不正な値が入力されています。',
            'sex.between' => '【性別】存在する項目から選択してください。',
            'comment.max' => '【コメント】100文字以内で入力してください。',
            'hobby.required' => '【趣味】は必須項目です。',
        ];
    }
}
