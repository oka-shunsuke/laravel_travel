<?php

namespace App\Http\Controllers;

use App\Models\Person;
use App\Models\Gender;
use App\Models\User_hobby;
use App\Models\Hobby;
use App\Models\Prefecture;
use App\Http\Requests\ValidRequest;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class PersonController extends Controller
{

    public function index( Request $request){
        $data = new Person;
        $users = $data->search($request->keyword);
        return view('person.index')->with([
            'users' => $users,
            'request' => $request
        ]);
    }//←トップページ//


    public function input( Request $request ){
        $pref = Prefecture::all();
        $sex = Gender::all();
        $hob = Hobby::all();
        
        //編集処理の場合は、下記のデータを保持
        $hob_box = [];
        $userID = $request->id ?: ''; 
        $checkName = $request->name ?: '';
        $u_data = Person::find($userID); 

        if(isset($request->back)){
            $u_data = $request;
            foreach($u_data->hobby as $hobb){
                $hob_box[] += $hobb;
            }

        }elseif( $userID !== "" ){
            if($u_data == null || $u_data->name !== $checkName ){
                return redirect(route("input"))->with('message', "不正なリクエストを検出したため[編集]処理を中断しました。");

            }else{
                foreach($u_data->user_hobbies as $hobb){
                    $hob_box[] += $hobb['hobby_id'];
                }
            }
        }
        return view('person.input')->with([
            'request' => $request,
            'pref'    => $pref,
            'sex'     => $sex, 
            'hob'     => $hob, 
            'user'    => $u_data, 
            'hob_box' => $hob_box   
        ]);
    }//←入力ページ//


    public function confirm( ValidRequest $request ) {
        $prefId = $request->prefecture;
        $sexID = $request->sex;
        $hobID = $request->hobby;
        $pref = Prefecture::find($prefId);
        $sex = Gender::find($sexID);
        $hob = Hobby::whereIn('id', $hobID)->get();
        return view('person.confirm')->with([
            'request' => $request, 
            'pref'    => $pref, 
            'sex'     => $sex, 
            'hob'     => $hob
        ]);
    }//←入力確認ページ//


    public function create( ValidRequest $request ){
        DB::beginTransaction();
        try {
            $data = new Person;
            $post_data = [
                'name' => $request->name,
                'prefecture' => $request->prefecture,
                'address' => $request->address,
                'address_second' => $request->address_second,
                'tel' => $request->tel,
                'sex' => $request->sex,
                'comment' => $request->comment,
            ];
            $data->fill($post_data)->save();
            $last_insert_id = $data->id;
            foreach($request->hobby as $hob){
                $hob_data = new User_hobby;
                $post_data = [
                    'hobby_id' => $hob,
                    'person_id' => $last_insert_id
                ];
                $hob_data->fill($post_data)->save();
            }
            DB::commit();
        } 
        catch(Exception $e1) {
            echo 'error' .$e->getMesseage;
            DB::rollback();
        }
        return view('person.conpleted');
    }//←新規登録処理/完了ページ//


    public function update( ValidRequest $request ){
        DB::beginTransaction();
        try {
            $post_data = [
                'name' => $request->name,
                'prefecture' => $request->prefecture,
                'address' => $request->address,
                'address_second' => $request->address_second,
                'tel' => $request->tel,
                'sex' => $request->sex,
                'comment' => $request->comment,
            ];
            $user_data = Person::find($request->id);
            $user_data->fill($post_data)->save();
            $hob = User_hobby::where( 'person_id', $request->id )->delete();
            foreach($request->hobby as $hob){
                $hob_data = new User_hobby;
                $post_data = [
                    'hobby_id' => $hob,
                    'person_id' => $request->id
                ];
                $hob_data->fill($post_data)->save();
            }
            DB::commit();
        } 
        catch(Exception $e1) {
            echo 'error' .$e->getMesseage;
            DB::rollback();
        }
        return view('person.conpleted');
    }//←更新処理/完了ページ//


    public function delete( Request $request ){
        DB::beginTransaction();
        try {
            $userID = $request->id ?: '';
            $checkName = $request->name ?: '';
            $u_data = Person::find($userID);
            if($u_data == null || $u_data->name !== $checkName ){
                return redirect(route("root"))->with('message', "不正なリクエストを検出したため[削除]処理を中断しました。");
            }
            Person::where('id' , $userID)->delete(); 
            User_hobby::where( 'person_id', $userID )->delete();
            DB::commit();
        } 
        catch(Exception $e1) {
            echo 'error' .$e->getMesseage;
            DB::rollback();
        }
        return view('person.conpleted');
    }//←削除処理/完了ページ//

}
