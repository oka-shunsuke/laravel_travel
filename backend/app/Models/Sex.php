<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sex extends Model
{
    protected $fillable = ['hob_name', 'sort_num'];
    use HasFactory;
}
