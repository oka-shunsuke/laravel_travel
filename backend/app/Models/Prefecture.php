<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prefecture extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['pre_name', 'sort_num'];


    public function people()
    {
        return $this->hasMany('App\Models\Person', 'id');
    }
}
