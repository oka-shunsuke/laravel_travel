<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hobby extends Model
{
    public $timestamps = false;
    protected $fillable = [ 'hob_name', 'sort_num' ];

    use HasFactory;

    public function user_hobbies()
    {
        return $this->hasMany('App\Models\User_hobby', 'hobby_id');
    }
}
