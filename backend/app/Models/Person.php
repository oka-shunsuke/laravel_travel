<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = [
        'name',
        'prefecture',
        'address',
        'address_second',
        'tel',
        'sex',
        'comment'
    ];

    public function prefectures()
    {
        return $this->belongsTo('App\Models\Prefecture', 'prefecture');
    }

    public function genders()
    {
        return $this->belongsTo('App\Models\Gender', 'sex');
    }

    public function user_hobbies()
    {
        return $this->hasMany('App\Models\User_hobby', 'person_id');
    }


    public function search( $keyword ){
        $search_person = $this::where('id', 'like', "%$keyword%")
            ->orWhere('name', 'like', "%$keyword%")
            ->orWhere('address', 'like', "%$keyword%")
            ->orWhere('address_second', 'like', "%$keyword%")
            ->orWhere('tel', 'like', "%$keyword%")
            ->orWhere('comment', 'like', "%$keyword%")
            ->orWhereHas('prefectures', function($q) use($keyword){
                $q->where('pre_name', 'like', "%$keyword%");
            })
            ->orWhereHas('genders', function($q) use($keyword){
                $q->where('sex', 'like', "%$keyword%");
            })
            ->orWhereHas('user_hobbies', function($q) use($keyword){
                $q->whereHas('hobbies', function($l) use($keyword){
                    $l->where('hob_name', 'like', "%$keyword%");
                });
            })
            ->paginate(config('const.People.MAX_RECORD')); 
        return $search_person;
    }
}
