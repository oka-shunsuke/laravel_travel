<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_hobby extends Model
{
    protected $fillable = ['hobby_id', 'person_id'];
    use HasFactory;

    public function hobbies()
    {
        return $this->belongsTo('App\Models\Hobby', 'hobby_id');
    }

    public function people()
    {
        return $this->belongsTo('App\Models\Person', 'person_id');
    }
}
