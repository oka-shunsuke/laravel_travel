<?php

return [
    
    'People' => [
        //ページネーションで一度に表示するページ数
        'MAX_PAGE' => 5,
        //一覧ページで一度に表示するレコード数
        'MAX_RECORD' => 10,
    ],

    
];  