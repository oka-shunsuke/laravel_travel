<?php

    use Illuminate\Support\Facades\Route;
    use App\Http\Controllers\PersonController;
    
    Route::get ('/',       [PersonController::class, 'index'])->name('root');
    Route::post('/',       [PersonController::class, 'index'])->name('search');
    Route::get ('input',   [PersonController::class, 'input'])->name('input');
    Route::post('check',   [PersonController::class, 'input'])->name('check'); 
    Route::post('confirm', [PersonController::class, 'confirm'])->name('confirm'); 
    Route::post('create',  [PersonController::class, 'create'])->name('create');
    Route::get ('edit',    [PersonController::class, 'input']);
    Route::post('edit',    [PersonController::class, 'input'])->name('edit');
    Route::post('update',  [PersonController::class, 'update'])->name('update');
    Route::post('delete',  [PersonController::class, 'delete'])->name('delete');
