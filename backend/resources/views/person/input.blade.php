<html>
    <head>
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">   
    </head>
    <body>
        <div class="error">
            <p class="error-text">{{$request->session()->get('message')}}</p>
        </div>
        
        @if ($errors->any())
        <div class="alert alert-danger mt-3">
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="error-text">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        
        <form name="form1" method="post" action="{{ route('confirm') }}" >
            @csrf
            名前：  <input type="text" name="name" value="{{old('name', isset($user->name) ? $user->name : '')}}" /><br />
            都道府県：<select name="prefecture" class="form-control">
                @foreach($pref as $pre)
                    <option value="{{$pre->id}}" @if( $pre->id == old('prefecture', isset($user->prefecture) ? $user->prefecture : '') ){{"selected"}}@endif  >
                    {{$pre->pre_name}}
                @endforeach
                </select><br />
            住所：  <input type="text" name="address"        value="{{old( 'address', isset($user->address) ? $user->address : '' )}}"/><br />
            住所2： <input type="text" name="address_second" value="{{old( 'address_second', isset($user->address_second) ? $user->address_second : '' )}}" /><br />
            電話番号:    <input type="text" name="tel"           value="{{old( 'tel', isset($user->tel) ? $user->tel : ''  )}}" /><br />
            性別:   @foreach($sex as $row)
                    <label>{{ $row["sex"] }}
                        <input class="form-control" type="radio" name="sex" value="{{ $row->id }}"
                        @if($row->id == old( 'sex', isset($user->sex) ? $user->sex : '' ) ){{"checked='checked'"}}@endif >
                    </label>
                    @endforeach <br />
            コメント：<input type="textarea" name="comment"  value="{{old( 'comment', isset($user->comment) ? $user->comment : '' )}}" /><br />
            趣味：  @foreach($hob as $row)
                    <label>{{ $row["hob_name"] }}
                        <input class="form-control" type="checkbox" name="hobby[]" value="{{ $row->id }}"
                        @if( in_array( $row["id"], (array)old('hobby', isset($hob_box) ? $hob_box : '') ) ){{"checked='checked'"}}@endif
                        >   
                    </label>
                    @endforeach <br />
            <input type="hidden" name="id" value=@if($user != "" ){{$user->id}}@endif>
            <input class="pointer" type="submit" value="送信">
            <input class="pointer" type="button" value="戻　る" onclick="location.href='{{ route('root') }}'">
        </form>
    </body>
</html>