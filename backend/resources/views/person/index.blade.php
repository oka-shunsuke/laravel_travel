<!DOCTYPE html>
<html lang="ja">
    <head>
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        <main class="padding-main">
            <div class="error">
                <p class="error-text">{{$request->session()->get('message')}}</p>
            </div>

            <div class="popup" id="popup">
				本当に削除しますか？<br />
				<div class="flex split yesORno">
					<button id="ok">削除する</button>
					<button id="no">キャンセル</button>
				</div>
			</div>

            <div class="home">
                <a href="{{ route('root') }}"><h1>トップへ戻る</h1></a>
            </div>

            <div class="flex symmetry-objects">
                <form name="form1" id="search1" method="post" action="{{ route('search') }}" >
                    @csrf
                    <input type="text" placeholder="キーワード" id="search-word" name="keyword" value="" />
                    <input class="pointer" type="submit" value="検索">	
                </form>

                <div class="page-count">
                    <p>{{ $users->currentPage() }}ページ</p>
                </div>
            </div>

            <table class="table-center wrap" border="1">
				<tr>
					<th>ID</th>
					<th>名前</th>	
					<th>都道府県</th>
					<th>住所</th>
					<th>住所2</th>  
					<th>TEL</th>
					<th>性別</th>
					<th>コメント</th>
					<th>趣味</th>
					<th>編集</th>
					<th>削除</th>
				</tr>

                @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->prefectures->pre_name }}</td>
                    <td>{{ $user->address }}</td>
                    <td>{{ $user->address_second }}</td>
                    <td>{{ $user->tel }}</td>
                    <td>{{ $user->genders->sex }}</td>
                    <td>{{ $user->comment }}</td>
                    <td>
                        @foreach ($user->user_hobbies as $hob)
                            {{ $hob->hobbies['hob_name'] }}
                        @endforeach 
                    </td>
                    <td>
                        <button class="pointer edit" data-name="{{ $user->name }}" data-id={{ $user->id }} >編集</button>
                    </td>
                    <td>
                        <button class="pointer remove" data-name="{{ $user->name }}" data-id="{{ $user->id }}" >削除</button>
                    </td>
                </tr>
                @endforeach
			</table>
            
            <div class="flex symmetry-objects">
                @if ($users->lastPage() > 1)
                <div class="pagination flex">
                    {{-- page-back --}}
                    <div class="page-item">
                        <a class="page-link{{ ($users->currentPage() == 1) ? ' disabled' : '' }}" href="{{ $users->url($users->currentPage()-1) }}">
                            <span aria-hidden="true">«</span>
                        </a>
                    </div>

                    {{-- page-count --}}
                    <div class="page-count">
                        @if( $users->lastPage() <= config('const.People.MAX_PAGE') )
                            @for ($i = 1; $i <= $users->lastPage(); $i++)
                                <a class="page-link{{ ($users->currentPage() == $i) ? ' disabled' : '' }}" href="{{ $users->url( $i ) }}" >
                                    <span class="count-box pointer" data-page={{ $i }} > {{ $i }} </span>
                                </a>
                            @endfor
                        @else
                            @if($users->currentPage() >= ($users->lastPage() - 2))
                                @for ($i = $users->lastPage() - 4; $i <= $users->lastPage(); $i++)
                                    <a class="page-link{{ ($users->currentPage() == $i) ? ' disabled' : '' }}" href="{{ $users->url( $i ) }}" >
                                        <span class="count-box pointer" data-page={{ $i }} > {{ $i }} </span>
                                    </a>
                                @endfor
                            @else
                                @for ($i = 1; $i <= config('const.People.MAX_PAGE'); $i++)
                                    @if( $users->currentPage() <= 3 )
                                        <a class="page-link{{ ($users->currentPage() == $i) ? ' disabled' : '' }}" href="{{ $users->url( $i ) }}" >
                                            <span class="count-box pointer" data-page={{ $i }} > {{ $i }} </span>
                                        </a>
                                    @else
                                        <?php $num = $users->currentPage() - 3; ?>
                                        <a class="page-link{{ ( $num + $i == $users->currentPage() ) ? ' disabled' : '' }}" href="{{ $users->url( $num + $i ) }}" >
                                            <span class="count-box pointer" data-page={{ $num + $i }} > {{ $num + $i }} </span>
                                        </a>
                                    @endif
                                @endfor 
                            @endif
                        @endif
                    </div>
                    
                    {{-- page-front --}}
                    <div class="page-item">
                        <a class="page-link{{ ($users->currentPage() == $users->lastPage()) ? ' disabled' : '' }}" href="{{ $users->url($users->currentPage()+1) }}" >
                            <span aria-hidden="true">»</span>
                        </a>
                    </div>
                </div>
                @endif

                <div class="postLink">
                    <button class="pointer" type="button" onclick="location.href='{{ route('input') }}'">新規作成</button>
                </div>
            </div>
        </main>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
		<script>

            $('.edit').click(function(){
				$id = $(this).data('id');
                $check_name = $(this).data('name');
                $('#search-word').append('<input type="text" name="id" value="' + $id + '">');
                $('#search-word').append('<input type="text" name="name" value="' + $check_name + '">');
                $('#search1').attr('action', '{{route("edit")}}' );
                $('#search1').submit();
			});

            $('.remove').click(function(){
                $('#popup').css('display', 'block')
                $id = $(this).data('id');
                $check_name = $(this).data('name');
                return false;
            });

            $('#ok').click(function(){
                $('#search-word').append('<input type="text" name="id" value="' + $id + '">');
                $('#search-word').append('<input type="text" name="name" value="' + $check_name + '">');
                $('#search1').attr('action', '{{route("delete")}}' );
                $('#search1').submit();
            });

            $('#no').click(function(){
                $('#popup').css('display', 'none');
            });

        </script>
    </body>
</html>