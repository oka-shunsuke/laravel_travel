<html>
	<head>
		<meta name="csrf-token" content="{{ csrf_token() }}"> 
	</head>
	<body>
		<form name="form1" id="form1" method="post">
            @csrf
			<input type="hidden" name="name"           value="{{$request->name}}" />
			<input type="hidden" name="prefecture" value="{{$request->prefecture}}" />
			<input type="hidden" name="address"        value="{{$request->address}}" />
			<input type="hidden" name="address_second" value="{{$request->address_second}}" />
			<input type="hidden" name="tel"   value="{{$request->tel}}" />
			<input type="hidden" name="sex" value="{{$request->sex}}" />
			<input type="hidden" name="comment" value="{{$request->comment}}" />
			@foreach($request->hobby as $hobb)
				<input type="hidden" name="hobby[]" value="{{$hobb}}" />
			@endforeach

			<input type="hidden" name="id" value="@isset($request->id){{$request->id}}@endisset">

			名前:	 {{$request->name}}<br />
			都道府県:{{$pref->pre_name}}<br />
			住所:	 {{$request->address}}<br />
			住所2:	 {{$request->address_second}}<br />
			TEL:	 {{$request->tel}}<br />
			性別:	 {{$sex->sex}}<br />
			コメント：{{$request->comment}}<br />
			趣味：
			@foreach($hob as $row)
				{{$row['hob_name']}}
			@endforeach
			<br />	

			<input class="pointer" type="submit" value="送信" formaction="@if($request->id != ""){{ route('update') }}@else{{ route('create')}}@endif" />
            <input class="pointer" type="submit" value="戻　る" formaction="{{ route('check', 'back=true') }}" />

		</form>	
	</body>
</html>
