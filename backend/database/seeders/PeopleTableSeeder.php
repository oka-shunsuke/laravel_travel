<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $param = [
            'name' => '山田太郎',
            'prefecture' => '6',
            'address' => 'テスト市',
            'address_second' => 'テスト町',
            'tel' => '000-000-000',
            'sex' => '1',
            'comment' => 'サンプル',
        ]
    }
}
